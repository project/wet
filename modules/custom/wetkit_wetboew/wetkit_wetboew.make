; WetKit WETBOEW Makefile

api = 2
core = 7.x

; Library for the Web Experience Toolkit jQuery Framework

libraries[wet-boew][download][type] = git
libraries[wet-boew][download][branch] = master-dist
libraries[wet-boew][download][revision] = 44ae375
libraries[wet-boew][download][url] = https://github.com/wet-boew/wet-boew-dist.git
